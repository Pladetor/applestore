package com.example.Danielapplestore;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.squareup.picasso.Picasso;


import java.util.ArrayList;

/**
 * this adapter hepls to decide how a row will look in the listview of the shop
 * i chose to show a picture on the left
 * then to show the name first and the price and details down the name
 * @author shir
 */
public class MyAdapter extends ArrayAdapter<Product> {

    private ArrayList<Product> objects = new ArrayList<Product>();
    private Context context;

    /**
     * this function defines the var we are going to work with in the next function
     * @param context The current context. This value cannot be null.
     * @param resource The resource ID for a layout file containing a TextView to use when instantiating views.
     * @param objects The resource ID for a layout file containing a TextView to use when instantiating views.
     */
    public MyAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Product> objects) {
        super(context, resource, objects);

        this.objects = objects;
        this.context = context;
    }
    /**
     * this function is connecting the layout to the java
     * @param position The position of the item within the adapter's data set of the item whose view we want.
     * @param convertView The old view to reuse, if possible.
     * @param parent The parent that this view will eventually be attached to
     * @return view of the listview by the "my_row"
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.my_row, null);

        TextView tvname = (TextView) v.findViewById(R.id.tvname);
        TextView tvdetails = (TextView) v.findViewById(R.id.tvdetails);
        ImageView imageView = (ImageView) v.findViewById(R.id.imageViewshop);

        tvname.setText(objects.get(position).getName());
        tvdetails.setText("details: " + objects.get(position).getDetails()+ "   "+ "price: " + objects.get(position).getPrice());

        if(objects.get(position).pic.isEmpty())
        {
            imageView.setImageResource(R.drawable.iphone);
        }
        else {
            Picasso.get().load(objects.get(position).pic).into(imageView);
        }

        return v;
    }
}
