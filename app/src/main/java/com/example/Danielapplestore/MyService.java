package com.example.Danielapplestore;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;


import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * this service sends notification to the user when he buys a new product from the store
 * the service is always running in the back of the application
 * even if the application is closed
 * @author Shir
 */

public class MyService extends Service {

    private FirebaseDatabase database = FirebaseDatabase.getInstance("https://applestore-4646a-default-rtdb.europe-west1.firebasedatabase.app/");
    private DatabaseReference myRef = database.getReference();

    private String userIdentifier;

    private Query q;

    //Build a.png new notification - Builder
    private NotificationCompat.Builder builder;
    private NotificationManager manager;

    @Override
    public void onCreate() {
        super.onCreate();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Service channel";
            String description = "Service channel description";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("my_channel_01", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        //Build a.png new notification - Builder
        builder = new NotificationCompat.Builder(MyService.this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setChannelId("my_channel_01")
                .setContentTitle("You have a new message in the firebase");


        //What is the intention for this notification?
        Intent notificationIntent = new Intent(MyService.this, MainActivity.class);

        //Postpone the intent using pendingIntent
        PendingIntent contentIntent = PendingIntent.getActivity(MyService.this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(contentIntent);

        // Add as notification
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        q = myRef.child("Users").child(((Configuration) this.getApplication()).getUserIdentifier()).child("Cart").orderByValue();

        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                builder.setContentText("Thank you for buying for the " + snapshot.getChildrenCount() + " time !");
                manager.notify(0, builder.build());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }
    public IBinder onUnBind(Intent arg0) {
        // TO DO Auto-generated method
        return null;
    }

    public void onStop() {}
    public void onPause() {}
    @Override
    public void onDestroy() {}
    @Override
    public void onLowMemory() {}
}