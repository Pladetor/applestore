package com.example.Danielapplestore;

import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * this activity presents the shop with the product in a listview
 * in every row we will se a product.
 * on the shop we will have an option to choose a product to enlarge
 * @author Shir
 */
public class ShopActivity extends AppCompatActivity {
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    private ListView shoplv;
    private Button cheapest;
    private ArrayList<Product> shopAl = new ArrayList<Product>();

    /**
     * this function responsible of showing the products
     * all the products in the store will be showen from the firebase with details and picture
     * also responsible for connecting the buttons and take to single product activity by press
     * @param savedInstanceState will help the activity to start the user interface
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);


        shoplv = (ListView) findViewById(R.id.shoplv) ;
        cheapest = (Button) findViewById(R.id.cheapest);

        this.database = FirebaseDatabase.getInstance("https://applestore-4646a-default-rtdb.europe-west1.firebasedatabase.app/");
        this.myRef = database.getReference();

        shoplv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                refreshLv();
                Intent i = new Intent(ShopActivity.this, Single_product_Activity.class);
                i.putExtra("value", parent.getItemAtPosition(position).toString());
                i.putExtra("userID", ((Configuration) getApplication()).getUserIdentifier());
                startActivity(i);

            }
        });

        cheapest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoplv.setAdapter(null);
            }
        });

        addItems();
}




    /**
     * this function will fill the listview with the products from the firebase
     */
    private void addItems () {
        Query q = myRef.child("Products").orderByValue();

        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dst: dataSnapshot.getChildren()){
                    Product p = dst.getValue(Product.class);
                    shopAl.add(p);
                }
                refreshLv();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    /**
     * this function refreshing the listview and cleaning the adapter after every use /change
     */
    private void refreshLv() {
        MyAdapter shopadp = new MyAdapter(this, R.layout.row , shopAl);
        shoplv.setAdapter(shopadp);
    }
}