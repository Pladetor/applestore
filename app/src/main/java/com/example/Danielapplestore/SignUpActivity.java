package com.example.Danielapplestore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * this activity is responsible of creating a new user for the client
 * every new user will have a name ,password ,email and phone
 * also checking in the firebase before createing to avoid duplicates
 * @author Shir
 */

public class SignUpActivity extends AppCompatActivity {

    private FirebaseDatabase database = FirebaseDatabase.getInstance("https://applestore-4646a-default-rtdb.europe-west1.firebasedatabase.app/");
    private DatabaseReference myRef = database.getReference("Users");

    private BroadcastInternet bcr;

    private EditText usernamesignup;
    private EditText passwordsignup;
    private EditText emailsignup;
    private EditText phonesignup;

    private String username;
    private String password;
    private String email;
    private String phone;

    /**
     * this function is responsible of connecting all the buttons
     * also it does all the tests of the signup infront of the firebase.
     * @param savedInstanceState will help the activity to start the user interface
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        this.bcr = new BroadcastInternet();

        Button signUpbtn = (Button) findViewById(R.id.signUpbtn);

        signUpbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernamesignup = (EditText) findViewById(R.id.nameet);
                passwordsignup = (EditText) findViewById(R.id.passwordet);
                emailsignup = (EditText) findViewById(R.id.emailet);
                phonesignup = (EditText) findViewById(R.id.phoneet);

                username = usernamesignup.getText().toString();
                password = passwordsignup.getText().toString();
                email = emailsignup.getText().toString();
                phone = phonesignup.getText().toString();

                if(!phone.matches("^[+]?[0-9]{10,13}$"))
                {
                    Toast.makeText(SignUpActivity.this, "Phone number is invalid", Toast.LENGTH_SHORT).show();
                }
                else if (passwordsignup.getText().toString().equals("") || usernamesignup.getText().toString().equals("") || emailsignup.getText().toString().equals("") || phonesignup.getText().toString().equals("")) {
                    Toast toast = Toast.makeText(SignUpActivity.this, "Please fill in the missing fields", Toast.LENGTH_SHORT);
                    toast.show();
                }
                else{
                    Query q = myRef.child("Users").orderByValue();
                    q.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            boolean userExist = false;
                            for (DataSnapshot dst : dataSnapshot.getChildren()) {
                                User u = dst.getValue(User.class);
                                if (u.getName().equals(username)) {
                                    userExist = true;
                                    Toast toast = Toast.makeText(SignUpActivity.this, "User already exists", Toast.LENGTH_SHORT);
                                    toast.show();
                                }
                            }
                            if (userExist == false) {
                                write_to_firebase(username, password, email, phone);
                                Intent i = new Intent(SignUpActivity.this, Login_Signup.class);
                                startActivity(i);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Toast.makeText(SignUpActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }


        });

    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.
     */
    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(this.bcr, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(this.bcr);
    }

    /**
     * this function is responsible of the writing into the firebase
     * after using that function we can see in the firebase the new user from the signup.
     * @param username the username of the new user
     * @param password the password of the new user
     * @param email the email of the new user
     * @param phone the phone of the new user
     */
    public void write_to_firebase(String username, String password,String email, String phone) {
        User u = new User(username, Integer.parseInt(password), email, phone);

        myRef.push().setValue(u);
    }
}