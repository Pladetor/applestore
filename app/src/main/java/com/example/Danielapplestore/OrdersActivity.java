package com.example.Danielapplestore;

import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * this activity will show the user all of hes orders during the time he was on the app
 * every order have the products the client bought
 * also every order have the date and time of buying it
 * @author Shir
 */
public class OrdersActivity extends AppCompatActivity {
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    private ListView lv;
    private ArrayList<String> orders = new ArrayList<String>();

    /**
     * the on create is called first when the activity is showed
     * @param savedInstanceState will help the activity to start the user interface
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);


        this.lv = (ListView) findViewById(R.id.orderslv);

        this.database = FirebaseDatabase.getInstance("https://applestore-4646a-default-rtdb.europe-west1.firebasedatabase.app/");
        this.myRef = database.getReference();

        addItems();

    }

    /**
     * this function will fill the listview with the orders from the firebase
     */
    private void addItems () {
        Query q = myRef.child("Users").child(((Configuration) this.getApplication()).getUserIdentifier()).child("Orders").orderByValue();

        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dst: dataSnapshot.getChildren()){
                    Order ord = dst.getValue(Order.class);
                    assert ord != null;
                    orders.add(ord.toString());
                }
                refreshLv();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    /**
     * this function refreshing the listview and cleaning the adapter after evrey use /change
     */
    private void refreshLv() {
        ArrayAdapter<String> ordersAdp = new ArrayAdapter<String>(this, R.layout.row, orders);
        this.lv.setAdapter(ordersAdp);
    }
}