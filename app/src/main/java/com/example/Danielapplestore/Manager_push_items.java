package com.example.Danielapplestore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

/**
 * this activity will let the manager to push new products to the firebase store
 * also the manager can take a pic that will save in the storage
 * every item that will be pushed to the firebase, will appear in the shop
 * @author shir
 */
public class Manager_push_items extends AppCompatActivity {

    private FirebaseDatabase database = FirebaseDatabase.getInstance("https://applestore-4646a-default-rtdb.europe-west1.firebasedatabase.app/");
    private DatabaseReference myRef = database.getReference("Products");

    private EditText managername;
    private EditText managerprice;
    private EditText managerdeta;

    private String name;
    private String price;
    private String details;

    private String pic_url = "";

    private StorageReference storageRef = FirebaseStorage.getInstance().getReference();

    private ImageView imageView;

    /**
     * this function is responsible of connecting the button and the edittext
     * also convert the data to string and pushing him into the firebase
     * @param savedInstanceState will help the activity to start the user interface
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_push_items);

        Button pushItemBtn = (Button) findViewById(R.id.MpDbtn);
        Button takePicbtn = (Button) findViewById(R.id.takePhotobtn);

        managername = (EditText) findViewById(R.id.managername);
        managerprice = (EditText) findViewById(R.id.managerprice);
        managerdeta = (EditText) findViewById(R.id.managerdeta);
        imageView = (ImageView) findViewById(R.id.imageView);

        pushItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            /**
             * the function waits for a push item click to push the new product to the firebase
             */
            public void onClick(View v) {
                name = managername.getText().toString();
                price = managerprice.getText().toString();
                details = managerdeta.getText().toString();

                write_to_firebase(name,price,details,pic_url);
            }
        });

        takePicbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            /**
             * the function waits for a button push to start the take picture process
             */
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, 123);
                }
            }
        });
    }

    /**
     * this function is responsible of setting the picture we took in the image view
     * @param requestCode identifies which app returned these results. This is defined by you when you call
     * @param resultCode informs you whether this app succeeded, failed, or something different
     * @param data holds any information returned by this app.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 123 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);

            //saving to storage
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data2 = baos.toByteArray();

            String photoname;
            photoname = managername.getText().toString();

            UploadTask uploadTask = storageRef.child("photos").child(photoname).putBytes(data2);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }

            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    pic_url = taskSnapshot.getStorage().getDownloadUrl().toString();
                }
            });
        }
    }

    /**
     * this function is writing all the data she gets into the firebase
     * so we can manage to do other things with it later
     * @param name the name of the new product
     * @param price the price of the new product
     * @param details the details of the new product
     * @param pic the pic of the new product
     */
    public void write_to_firebase(String name, String price,String details, String pic) {
        Product p = new Product(name,Integer.parseInt(price),details,pic);

        myRef.push().setValue(p);
    }
}