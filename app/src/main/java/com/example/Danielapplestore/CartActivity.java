package com.example.Danielapplestore;

import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.ListView;


import java.util.ArrayList;

/**
 * this activity will show all the products in the order
 * the cart will save all the users products even when they closed the app
 * the cart also will give the amount of money to pay
 * @author Shir
 */
public class CartActivity extends AppCompatActivity {

    private ListView cartlv;
    private ArrayList<Product> cartAl = new ArrayList<Product>();
    private Button cartbtn;
    private Button refreshcart;


    private FirebaseDatabase database;
    private DatabaseReference myRef;

    /**
     * this function is connecting the button and the listview of the activity
     * @param savedInstanceState  will help the activity to start the user interface,
     * If no data was supplied, savedInstanceState is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        this.database = FirebaseDatabase.getInstance("https://applestore-4646a-default-rtdb.europe-west1.firebasedatabase.app/");
        this.myRef = database.getReference();

        cartbtn = (Button) findViewById(R.id.cartbtn);
        refreshcart = (Button) findViewById(R.id.refreshcart);
        cartlv = (ListView) findViewById(R.id.cartlv);

        cartbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CartActivity.this, PayActivity.class);
                i.putExtra("userID", ((Configuration) getApplication()).getUserIdentifier());
                startActivity(i);
            }
        });

        refreshcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshLv();
            }
        });

        addItems();
        refreshLv();
    }

    /**
     * this function refreshing the listview and cleaning the adapter after every use /change
     */
    private void refreshLv() {
        MyAdapter cartadp = new MyAdapter(this, R.layout.row, this.cartAl);
        this.cartlv.setAdapter(cartadp);
    }

    /**
     * this function will fill the listview with the products from the firebase
     */
    private void addItems () {
        Query q = myRef.child("Users").child(((Configuration) this.getApplication()).getUserIdentifier()).child("Cart").orderByValue();

        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dst: dataSnapshot.getChildren()){
                    Product p = dst.getValue(Product.class);
                    cartAl.add(p);
                }
                refreshLv();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}