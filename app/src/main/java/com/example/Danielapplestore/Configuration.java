package com.example.Danielapplestore;

import android.app.Application;

/**
 * this java is responsible of getting the user identifier
 * this key tells all the application witch user is using the application right know
 * every activity have to get this key first thing before the rest of the code
 * this java runs first because of the extend from the Application and not AppCompatActivity.
 * @author Shir
 */
public class Configuration extends Application {
    private String userIdentifier;

    public String getUserIdentifier() {
        return this.userIdentifier;
    }

    public void setUserIdentifier(String id) {
        this.userIdentifier = id;
    }
}
