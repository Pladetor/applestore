package com.example.Danielapplestore;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

/**
 * this activity will show the user the single item he clicked on in the shop
 * the name price and details will be shown bigger
 * also a picture will be on the screen
 * @author shir
 */
public class Single_product_Activity extends AppCompatActivity {
    private Button ToCart;
    private TextView details;
    private TextView name;
    private TextView price;
    private ImageView iv;

    private FirebaseDatabase database;
    private DatabaseReference myRef;


    /**
     * this function is responsible of connecting the textviews and buttons
     * @param savedInstanceState will help the activity to start the user interface
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_product);

        this.database = FirebaseDatabase.getInstance("https://applestore-4646a-default-rtdb.europe-west1.firebasedatabase.app/");
        this.myRef = database.getReference();

        details = (TextView) findViewById(R.id.detatv);
        name = (TextView) findViewById(R.id.nametv);
        price = (TextView) findViewById(R.id.pricetv);
        ToCart = (Button) findViewById(R.id.buttonSingleItem);
        iv = (ImageView) findViewById(R.id.iv);

        String str = getIntent().getExtras().getString("value");

        Product p = new Product(str);

        name.setText(p.getName());
        price.setText(String.valueOf(p.getPrice()));
        details.setText(p.getDetails());

        if(p.pic.isEmpty())
        {
            iv.setImageResource(R.drawable.iphone);
        }
        else {
            Picasso.get().load(p.pic).into(iv);
        }

        ToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Query q = myRef.child("Users").child(((Configuration) getApplication()).getUserIdentifier()).child("Cart").orderByValue();

                q.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        myRef.child("Users").child(((Configuration) getApplication()).getUserIdentifier()).child("Cart").push().setValue(p);

                        Intent i = new Intent(Single_product_Activity.this, CartActivity.class);
                        i.putExtra("userID", ((Configuration) getApplication()).getUserIdentifier());
                        startActivity(i);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });

    }

}