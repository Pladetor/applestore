package com.example.Danielapplestore;

import java.util.ArrayList;
import java.util.Date;

/**
 * this java document describe how an order will look like after a user will buy something.
 * every order have the products
 * also every order have a date and time to manage when the order will deliver
 * @author Shir
 */
public class Order {

    private ArrayList<Product> products;
    private Date date;

    public Order(ArrayList<Product> _products) {
        this.products = _products;
        this.date = new Date();
    }


    public Order() {
    }

    /**
     * this function is casting every order to a string
     * @return a string represents the order
     */
    @Override
    public String toString() {
        String s = "date: " + '\'' + this.date.toString() + "\'\n";
        for (int i = 0; i<this.products.size(); i++)
        {
            s += "product" + i + ": " + this.products.get(i).getName() + "\n";
        }

        return s;
    }

    /**
     * returns the array list when called
     * @return the array list of the product
     */

    public ArrayList<Product> getProducts() {
        return products;
    }

    /**
     * sets the product in the array list after buying them.
     * @param products the products the client bought.
     */

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    /**
     * returns the date and time when ordering the order
     * @return the date and time
     */

    public Date getDate() {
        return date;
    }

    /**
     * sets the date and time after buying products
     * @param date the date and time of the order
     */

    public void setDate(Date date) {
        this.date = date;
    }
}