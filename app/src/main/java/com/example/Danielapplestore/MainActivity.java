package com.example.Danielapplestore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * this is the main activity of my project
 * responsible for opening video and navigation menu
 * @author Daniel
 */
public class MainActivity extends AppCompatActivity {

    private Button btn;
    private VideoView vid;
    private MediaController m;
    private String userIdentifier;


    /**
     * this activity will show the user a short video of the products in the store
     * @param savedInstanceState will help the activity to start the user interface
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        vid = (VideoView)findViewById(R.id.vid);
        vid.setVideoPath("android.resource://" + getPackageName() + "/"+ R.raw.sample);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(vid);
        vid.setMediaController(mediaController);

    }

    /**
     * this activity will show the user the menu of the store and manage the movement in the activites
     * @param menu holds the new menu info we set
     * @return the new menu we sets and created
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.shopmenu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * this function will manage the activities the user can go to
     * @param item holds the layout item we point to
     * @return the new activity layout that we clicked on in the menu
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.shop:
                Intent q = new Intent(MainActivity.this,ShopActivity.class);
                q.putExtra("userID", ((Configuration) this.getApplication()).getUserIdentifier());
                startActivity(q);
                break;
            case R.id.cart:
                Intent a = new Intent(MainActivity.this,CartActivity.class);
                a.putExtra("userID", ((Configuration) this.getApplication()).getUserIdentifier());
                startActivity(a);
                break;
            case R.id.orders:
                Intent b = new Intent(MainActivity.this,OrdersActivity.class);
                b.putExtra("userID", ((Configuration) this.getApplication()).getUserIdentifier());
                startActivity(b);
                break;
            case R.id.exit:
                finishAffinity();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}