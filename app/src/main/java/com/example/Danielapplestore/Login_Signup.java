package com.example.Danielapplestore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * this activity is doing the login of the user with his name and password,
 * if the client is new this activity will send him to creat a new user.
 * also in the activity i have Input tests for the edit texts
 * @author shir
 */

public class Login_Signup extends AppCompatActivity {

    private FirebaseDatabase database;
    private DatabaseReference myRef;

    private EditText usernamelogin;
    private EditText passwordlogin;
    private Button loginbtn;
    private Button signupbtn;

    /**
     * this function is responsible of checking is the user the client field is exist
     * if the user doesnt exist the client will get a massage to create one.
     * also this function is doing all the buttons connection.
     * @param savedInstanceState will help the activity to start the user interface,
     * If no data was supplied, savedInstanceState is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);
        this.database= FirebaseDatabase.getInstance("https://applestore-4646a-default-rtdb.europe-west1.firebasedatabase.app/");
        this.myRef = database.getReference();

        usernamelogin = (EditText) findViewById(R.id.usernamelogin);
        passwordlogin = (EditText) findViewById(R.id.passwordlogin);
        loginbtn = (Button) findViewById(R.id.loginbtn);
        signupbtn = (Button) findViewById(R.id.signUpbtn);


        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            /**
             * this function is listening to a click of a button from the lout
             */
            public void onClick(View v) {

                String username = usernamelogin.getText().toString();
                String password = passwordlogin.getText().toString();

                if (passwordlogin.getText().toString().equals("") || usernamelogin.getText().toString().equals("")) {
                    Toast toast = Toast.makeText(Login_Signup.this, "Please fill in the missing fields", Toast.LENGTH_SHORT);
                    toast.show();

                }
                if (username.equals("daniel1") || password.equals("1234")) {
                    Intent i = new Intent(Login_Signup.this, Manager_push_items.class);
                    startActivity(i);
                }
                    else {
                        Query q = myRef.child("Users").orderByValue();

                        q.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            /**
                             * this function waits for a change to make in the firebase and also
                             * do a data tests on the info we got from the firebase and the edit texts together
                             */
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                for (DataSnapshot dst : dataSnapshot.getChildren()) {
                                    User u = dst.getValue(User.class);
                                    String name = u.getName();
                                    int pass = u.getPassword();
                                    if (name.equals(username) && pass == Integer.parseInt(password)) {
                                        Intent i = new Intent(Login_Signup.this, MainActivity.class);
                                        i.putExtra("userID", dst.getKey());
                                        ((Configuration) getApplication()).setUserIdentifier(dst.getKey());
                                        startActivity(i);

                                        return;
                                    }
                                }

                                Toast toast = Toast.makeText(Login_Signup.this, "Login credentials are incorrect", Toast.LENGTH_SHORT);
                                toast.show();
                                Intent i = new Intent(Login_Signup.this, Login_Signup.class);
                                startActivity(i);
                            }
                            @Override
                            /**
                             * this function trys to catch errors from the firebase
                             */
                            public void onCancelled(DatabaseError databaseError) {

                            }

                        });

                    }
                }
        });


        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login_Signup.this, SignUpActivity.class);
                startActivity(i);
            }
        });
    }

}