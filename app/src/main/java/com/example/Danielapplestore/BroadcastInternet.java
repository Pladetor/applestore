package com.example.Danielapplestore;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.widget.Toast;

/**
 * this activity made for checking if the user has connection for the internet
 * if the user doesn't have connection there will be a toast so he will know on the login page
 * @author Shir
 */
public class BroadcastInternet extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction()))
        {
            boolean noConnection = intent.getBooleanExtra(
                    ConnectivityManager.EXTRA_NO_CONNECTIVITY, false
            );

            if(noConnection){
                Toast.makeText(context, "Machine is not connected to the network",
                        Toast.LENGTH_SHORT).show();

            }
            else{
                Toast.makeText(context, "Connected to the network",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}