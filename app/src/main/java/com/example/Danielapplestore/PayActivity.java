package com.example.Danielapplestore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * this activity will help you decide how to pay
 * also get your address and credit card details
 * every pay needs  the user name, his email, his phone and the card to pay with.
 * @author Shir
 */
public class PayActivity extends AppCompatActivity {
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    private Button paybtn;
    private EditText nameEt;
    private EditText emailEt;
    private EditText phoneEt;
    private EditText cardEt;

    private Intent svc;

    /**
     * this function will inform the pay and confirm to you that you payed.
     * @param savedInstanceState will help the activity to start the user interface
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        this.database = FirebaseDatabase.getInstance("https://applestore-4646a-default-rtdb.europe-west1.firebasedatabase.app/");
        this.myRef = database.getReference();

        this.paybtn = (Button) findViewById(R.id.paybtn);
        this.nameEt = (EditText) findViewById(R.id.editTextTextPersonName2);
        this.emailEt = (EditText) findViewById(R.id.editTextTextEmailAddress2);
        this.phoneEt = (EditText) findViewById(R.id.editTextPhone2);
        this.cardEt = (EditText) findViewById(R.id.edittextcard);

        paybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkCredentials())
                {

                    pullProductsFromCart();

                    Toast toast = Toast.makeText(PayActivity.this,"You paid!" ,Toast.LENGTH_SHORT);
                    toast.show();

                    Intent i = new Intent(PayActivity.this, OrdersActivity.class);
                    i.putExtra("userID", ((Configuration) getApplication()).getUserIdentifier());
                    startActivity(i);
                }
                else
                {
                    Toast toast = Toast.makeText(PayActivity.this,"Invalid credentials!" ,Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
    }

    /**
     * This function will pull a user's Cart to an ArrayList
     */
    private void pullProductsFromCart()
    {
        Query q = this.myRef.child("Users").child(((Configuration) this.getApplication()).getUserIdentifier()).child("Cart").orderByValue();

        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Product> productsFromCart = new ArrayList<Product>();
                for(DataSnapshot dst: dataSnapshot.getChildren()){ // for each product in the cart
                    Product p = dst.getValue(Product.class);
                    productsFromCart.add(p);
                }

                registerNewOrder(new Order(productsFromCart));

                myRef.child("Users").child(((Configuration) getApplication()).getUserIdentifier()).child("Cart").removeValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    /**
     * The function will add a new order to the user in the firebase
    // * @param order the Order object to be added
     */
    private void registerNewOrder(Order order) {

        myRef.child("Users").child(((Configuration) this.getApplication()).getUserIdentifier()).child("Orders").push().setValue(order);
    }


    private boolean checkCredentials()
    {
        String card = this.cardEt.getText().toString();

        if (this.nameEt.getText().toString().equals("") ||
                this.phoneEt.getText().toString().equals("") ||
                this.cardEt.getText().toString().equals("") ||
                this.emailEt.getText().toString().equals(""))
        {
            return false;
        }
        else return card.matches("[0-9]+") && card.length() > 2;
    }

}