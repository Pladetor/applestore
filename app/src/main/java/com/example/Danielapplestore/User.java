package com.example.Danielapplestore;

/**
 * this java describes a user
 * the user have name password email and phone
 * with the user i can manage the clients above the application
 * @author shir
 */
public class User
{
    private String name;
    private int password;
    private String email;
    private  String phone;


    public User(){};

    public User(String name, int password, String email, String phone) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.phone = phone;
    }

    /**
     * this method will be run whenever we need to use the user
     * sets the email
     * @param email the email of the user
     */

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * this method will be run whenever we need to use the user
     * sets the phone
     * @param phone the phone of the user
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * this method will be run whenever the user is back from the firebase
     * returns the email
     * @return the email of the user
     */
    public String getEmail() {
        return email;
    }

    /**
     * this method will be run whenever the user is back from the firebase
     * returns the phone
     * @return the phone of the user
     */
    public String getPhone() {
        return phone;
    }

    /**
     * this method will be run whenever the user is back from the firebase
     * returns the name
     * @return the name of the user
     */
    public String getName() {
        return name;
    }

    /**
     * this method will be run whenever we need to use the user
     * sets the name
     * @param name the name of the user
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * this method will be run whenever the user is back from the firebase
     * returns the password
     * @return the password of the user
     */
    public int getPassword() {
        return password;
    }

    /**
     * this method will be run whenever we need to use the user
     * sets the password
     * @param password the password of the user
     */

    public void setPassword(int password) {
        this.password = password;
    }
}
