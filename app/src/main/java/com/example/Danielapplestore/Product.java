package com.example.Danielapplestore;

/**
 * this java is responsible of represent the product
 * every product has name, price, details and pic.
 * the products will be shown in the shop.
 * @author Shir
 */
public class Product {
    private String name;
    private int price;
    private String details;
    public String pic;

    public Product(String name, int price, String details, String pic) {
        this.name = name;
        this.price = price;
        this.details = details;
        this.pic = pic;
    }

    public Product(){}

    public Product(String init){
        // name: 'shir', price: 150, details: 'arrrr', pic: 'path'
        String str = init;
        this.name = str.substring(str.indexOf(':') + 3, str.indexOf(',') - 1);
        str = str.substring(str.indexOf(',') + 2);

        this.price = Integer.parseInt(str.substring(str.indexOf(':') + 2, str.indexOf(',')));
        str = str.substring(str.indexOf(',') + 2);

        this.details = str.substring(str.indexOf(':') + 3, str.indexOf(',') - 1);
        str = str.substring(str.indexOf(',') + 1);

        this.pic = str.substring(str.indexOf(':') + 3, str.lastIndexOf('\''));
    }

    /**
     * this method will be run whenever the product is back from the firebase
     * returns the pic of the product
     * @return pic of product
     */
    public String getPic() {
        return pic;
    }

    /**
     * this method will be run whenever i use the product
     * sets the pic
     * @param pic the pic of the product
     */
    public void setPic(String pic) {
        this.pic = pic;
    }

    /**
     * this method will be run whenever the product is back from the firebase
     * returns the name of the product
     * @returnthe the name of the product
     */
    public String getName() {
        return name;
    }

    /**
     * this method will be run whenever the product is back from the firebase
     * returns the price of the product
     * @return the price of the product
     */
    public int getPrice() {
        return price;
    }

    /**
     * this method will be run whenever the product is back from the firebase
     * returns the details of the product
     * @return the details of the product
     */
    public String getDetails() {
        return details;
    }

    /**
     * this method will be run whenever i use the product
     * sets the name of the product
     * @param name of the product
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * this method will be run whenever i use the product
     * sets the price of the product
     * @param price of the product
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * this method will be run whenever i use the product
     * sets the price of the product
     * @param details of the product
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * the function create a copy of this product
     * @return a copy of the product
     */
    public Product CreateCopy()
    {
        return new Product(this.name, this.price, this.details, this.pic);
    }
    @Override
    /**
     * this function is casting every product to string
     */
    public String toString() {
        return "name: " + '\'' + name + '\'' +
                ", price: " + price +
                ", details: " + '\'' + details + '\'' +
                ", pic: " + '\'' + pic + '\'';
    }

}
